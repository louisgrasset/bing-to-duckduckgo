// match pattern for the URLs to redirect
let searchTerms = "";
let pattern = "https://www.bing.com/*";

// redirect function returns a Promise
// which is resolved with the redirect URL when a timer expires
function redirectAsync(requestDetails) {
  let regex = "q=(.*?)(&|$)";
  let redirectUrl = "https://duckduckgo.com";

  if (requestDetails.url.match(regex)) {
	  searchTerms = requestDetails.url.match(regex);
	  searchTerms = searchTerms[1];
	  redirectUrl = redirectUrl + "/?q=" + searchTerms;
  }

  let asyncCancel = new Promise((resolve, reject) => {
    window.setTimeout(() => {
      resolve({redirectUrl});
    }, 10);
  });
    
  console.log(asyncCancel);
  return asyncCancel;
}

// add the listener,
// passing the filter argument and "blocking"
browser.webRequest.onBeforeRequest.addListener(
  redirectAsync,
  {urls: [pattern], types: ["main_frame"]}, 
  ["blocking"]
);